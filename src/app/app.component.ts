import { Component } from '@angular/core'
import { Clipboard, PendingCopy } from '@angular/cdk/clipboard'
import { ResumeService } from './resume.service'
import { Resume } from './common/resume'
import { MatSnackBar } from '@angular/material/snack-bar'

import { stringify } from 'yaml'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cv-generator'

  editorFlex: number = 60

  constructor(
    private clipBoard: Clipboard,
    private resumeService: ResumeService,
    private snackBar: MatSnackBar
  ) {
  }

  copyYaml() {
    const resume: Resume = this.resumeService.resume
    const contentToCopy: string = stringify(resume)
    const pendingCopy: PendingCopy = this.clipBoard.beginCopy(contentToCopy)

    pendingCopy.copy()

    this.snackBar.open(`Yaml copied`, undefined, {
      duration: 1500
    })


  }


}
