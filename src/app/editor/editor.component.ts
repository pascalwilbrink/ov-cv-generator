import { Component, OnInit } from '@angular/core'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { Resume } from '../common/resume'
import { ResumeService } from '../resume.service'

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  basicInfoFormGroup!: FormGroup
  employeeFormGroup!: FormGroup
  blocksFormGroup!: FormGroup
  skillsFormGroup!: FormGroup
  projectHistoryFormGroup!: FormGroup

  constructor(
    private fb: FormBuilder,
    private resumeService: ResumeService
  ) { 
    this._buildForms()

    this.resumeService
      .reload
      .subscribe({
        next: (res) => {
          if (res) {
            this._buildForms()
          }
        }
      })
  }

  private _buildForms() {
    const resume: Resume = this.resumeService.resume
    this.basicInfoFormGroup = this.fb.group({
      name: new FormControl(resume.name, [ Validators.required ]),
      title: new FormControl(resume.title, [ Validators.required ]),
      birthdate: new FormControl(resume.birthdate, [ Validators.required ]),
      residence: new FormControl(resume.residence, [ Validators.required ])
    })

    this.employeeFormGroup = this.fb.group({
      introduction: new FormControl(resume.employee.introduction.join('\n'), [ Validators.required ])
    })

    this.blocksFormGroup = this.fb.group({
      blocks: new FormArray(
        [
          new FormGroup({
            title: new FormControl(resume.block1.title, [ Validators.required ]),
            items: new FormArray(
              resume.block1.list.map((item) => {
                return new FormControl(item, [ Validators.required ])
              })
            )
          }),
          new FormGroup({
            title: new FormControl(resume.block2.title, [ Validators.required ]),
            items: new FormArray(
              resume.block2.list.map((item) => {
                return new FormControl(item, [ Validators.required ])
              })
            )
          })
        ]
      )
    })

    this.skillsFormGroup = this.fb.group({
      left_tables: new FormArray(
        resume.technical_skills.left_tables.map((table) => {
          table.title_code
          return new FormGroup({
            title_code: new FormControl(table.title_code, [ Validators.required ]),
            skills: new FormArray(
              table.skills.map((skill) => {
                return new FormGroup({
                  name: new FormControl(skill.name, [ Validators.required ]),
                  stars: new FormControl(skill.stars, [ Validators.required ]),
                  since: new FormControl(skill.since, [ Validators.required ])            
                })
              })
            )
          })
        })
      ),
      right_tables: new FormArray(
        resume.technical_skills.right_tables.map((table) => {
          table.title_code
          return new FormGroup({
            title_code: new FormControl(table.title_code, [ Validators.required ]),
            skills: new FormArray(
              table.skills.map((skill) => {
                return new FormGroup({
                  name: new FormControl(skill.name, [ Validators.required ]),
                  stars: new FormControl(skill.stars, [ Validators.required ]),
                  since: new FormControl(skill.since, [ Validators.required ])            
                })
              })
            )
          })
        })
      )
    })

    this.projectHistoryFormGroup = this.fb.group({
      break_before: new FormControl(resume.project_history.break_before),
      pages: new FormArray(
        resume.project_history.pages.map((page) => {
          return new FormGroup({
            aside: new FormControl(page.aside),
            content: new FormArray(
              page.content.map((content) => {
                return new FormGroup({
                  title: new FormControl(content.title, [ Validators.required ]),
                  period: new FormControl(content.period, [ Validators.required ]),
                  client: new FormControl(content.client, [ Validators.required ]),
                  description: new FormControl(content.description.join('\n'), [ Validators.required ])
                })
              })
            )
          })
        })
      )
    })
  }

  ngOnInit(): void {
  }

}
