import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { COMMA, ENTER } from '@angular/cdk/keycodes'

import { MatChipInputEvent } from '@angular/material/chips'

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  readonly separatorKeysCodes = [ ENTER, COMMA ] as const

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  technologies: string[] = []

  @Output()
  onDelete: EventEmitter<FormGroup> = new EventEmitter<FormGroup>()

  constructor() { }

  ngOnInit(): void {
  }

  addToProject($event: MatChipInputEvent) {
    this.technologies.push($event.value)
    $event.chipInput!.clear()
    this.updateResume()
  }

  removeFromProject(item: string) {
    this.technologies = this.technologies.filter((t) => t !== item)
    this.updateResume()
  }

  private updateResume() {
    this.formGroup.get('technologies')?.setValue(this.technologies)
  }

  deleteProject() {
    this.onDelete.emit(this.formGroup)
  }

}
