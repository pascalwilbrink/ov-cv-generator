import { Component, Input, OnInit } from '@angular/core'
import { FormArray, FormControl, FormGroup } from '@angular/forms'
import { Resume } from 'src/app/common/resume'
import { ResumeService } from 'src/app/resume.service'

@Component({
  selector: 'app-project-history',
  templateUrl: './project-history.component.html',
  styleUrls: ['./project-history.component.scss']
})
export class ProjectHistoryComponent implements OnInit {

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  constructor(
    private resumeService: ResumeService
  ) { }

  ngOnInit(): void {
    this.formGroup
      .statusChanges
      .subscribe({
        next: (res) => {
          this.updateResume()
        }
      })
  }

  get pages(): FormGroup[] {
    const pages: FormGroup[] = []

    const formArray: FormArray = this.formGroup.get('pages') as FormArray

    for (let i = 0; i < formArray.length; i++) {
      pages.push(formArray.at(i) as FormGroup)
    }

    return pages
  }

  addPage() {
    (this.formGroup.get('pages') as FormArray).push(new FormGroup({
      aside: new FormControl(false),
      content: new FormArray([])
    }))
  }

  removePage(index: number) {
    (this.formGroup.get('pages') as FormArray).removeAt(index)
  }

  onDeletePage(index: number) {
    this.removePage(index)
  }

  private updateResume() {
    const resume: Partial<Resume> = {}

    resume.project_history = {
      break_before: this.formGroup.controls['break_before'].value,
      pages: []
    }

    const pagesArray: FormArray = (this.formGroup.get('pages') as FormArray)

    for (let i = 0; i < pagesArray.length; i++ ) {
      const pageFormGroup: FormGroup = pagesArray.at(i) as FormGroup

      const content: any[] = []
      const contentArray: FormArray = (pageFormGroup.get('content') as FormArray)

      for (let j = 0; j < contentArray.length; j++) {
        const contentFormGroup: FormGroup = contentArray.at(j) as FormGroup

        content.push({
          title: contentFormGroup.get('title')?.value,
          period: contentFormGroup.get('period')?.value,
          client: contentFormGroup.get('client')?.value,
          description: contentFormGroup.get('description')?.value.split('\n'),
          technologies: contentFormGroup.get('technologies')?.value
        })
      }

      resume.project_history.pages.push({
        aside: pageFormGroup.controls['aside'].value,
        content: content
      })

    }

    console.log('resume', resume)
    this.resumeService.updateResume(resume) 
  }

}
