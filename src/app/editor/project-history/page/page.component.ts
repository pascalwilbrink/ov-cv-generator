import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { ResumeService } from 'src/app/resume.service'

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }
  
  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  @Output()
  onDelete: EventEmitter<FormGroup> = new EventEmitter<FormGroup>()

  constructor(
    private resumeService: ResumeService
  ) { }

  ngOnInit(): void {
  }

  get projects(): FormGroup[] {
    const projects: FormGroup[] = []

    const formArray: FormArray = this.formGroup.get('content') as FormArray

    for (let i = 0; i < formArray.length; i++) {
      projects.push(formArray.at(i) as FormGroup)
    }

    return projects;
  }

  addProject() {
    (this.formGroup.get('content') as FormArray).push(
      new FormGroup({
        title: new FormControl('', [ Validators.required ]),
        period: new FormControl('', [ Validators.required ]),
        client: new FormControl('', [ Validators.required ]),
        description: new FormControl('', [ Validators.required ]),
        technologies: new FormControl([])
      })
    )
  }

  onDeleteProject(project: FormGroup) {
    (this.formGroup.get('content') as FormArray).controls = (this.formGroup.get('content') as FormArray).controls.filter((c) => c !== project)
  }

  deletePage() {
    this.onDelete.emit(this.formGroup) 
  }

}
