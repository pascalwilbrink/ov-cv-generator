import { Component, Input, OnInit } from '@angular/core'

import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms'
import { Resume } from 'src/app/common/resume'
import { ResumeService } from 'src/app/resume.service'

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup
  
  get leftTables(): FormGroup[] {
      const tables: FormGroup[] = []

      if (this.formGroup) {
        for (let i = 0; i < this.leftTablesArray.length; i++) {
          tables.push(this.leftTablesArray.at(i) as FormGroup)
        }
      }

      return tables
  } 

  get rightTables(): FormGroup[] {
    const tables: FormGroup[] = []

    if (this.formGroup) {
      for (let i = 0; i < this.rightTablesArray.length; i++) {
        tables.push(this.rightTablesArray.at(i) as FormGroup)
      }
    }

    return tables
  }

  constructor(
    private resumeService: ResumeService
  ) { }

  ngOnInit(): void {
    this.formGroup
    .valueChanges
    .subscribe({
      next: () => {
        this.updateResume()
      }
    })
  }

  addSkillTable(position: 'left' | 'right') {
    const formGroup: FormGroup = new FormGroup({
      title_code: new FormControl('', [ Validators.required ]),
      skills: new FormArray([])
    })
    position === 'left' ? this.leftTablesArray.push(formGroup) : this.rightTablesArray.push(formGroup)
  }
 
  onDeleteSkillTable(table: FormGroup) {
    const isLeft = this.leftTables.includes(table)

    isLeft ? 
      this.leftTablesArray.controls = this.leftTablesArray.controls.filter((c) => c !== table) 
      : 
      this.rightTablesArray.controls = this.rightTablesArray.controls.filter((c) => c !== table)

    this.updateResume()
  }

  getFormArray(field: string): FormArray {
    return this.formGroup.get(field) as FormArray
  }

  get leftTablesArray(): FormArray {
    return this.getFormArray('left_tables')
  }

  get rightTablesArray(): FormArray {
    return this.getFormArray('right_tables')
  }

  private updateResume() {
    const resume: Partial<Resume> = {}

    resume.technical_skills = {
      left_tables: [],
      right_tables: []
    }

    this.leftTables
      .forEach((table) => {
        const skills: any[] = []
        for (let i = 0; i < (table.controls['skills'] as FormArray).length; i++) {
          const skillGroup: FormGroup = (table.controls['skills'] as FormArray).at(i) as FormGroup
          skills.push({
            name: skillGroup.controls['name'].value,
            stars: skillGroup.controls['stars'].value,
            since: skillGroup.controls['since'].value
          })
        }

        resume.technical_skills?.left_tables.push({
          title_code: table.controls['title_code'].value,
          skills: skills
        })
      })

    this.rightTables
      .forEach((table) => {
        const skills: any[] = []
        for (let i = 0; i < (table.controls['skills'] as FormArray).length; i++) {
          const skillGroup: FormGroup = (table.controls['skills'] as FormArray).at(i) as FormGroup
          skills.push({
            name: skillGroup.controls['name'].value,
            stars: skillGroup.controls['stars'].value,
            since: skillGroup.controls['since'].value
          })
        }

        resume.technical_skills?.right_tables.push({
          title_code: table.controls['title_code'].value,
          skills: skills
        })
      })

    this.resumeService.updateResume(resume)
  }

  onUpdate() {
    this.updateResume()
  }
}
