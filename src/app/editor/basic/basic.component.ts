import { Component, Input, OnInit } from '@angular/core'

import { FormGroup } from '@angular/forms'
import { Resume } from 'src/app/common/resume'
import { ResumeService } from 'src/app/resume.service'

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent implements OnInit {

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  constructor(
    private resumeService: ResumeService
  ) { 

  }

  ngOnInit(): void {
    this.formGroup
      .valueChanges
      .subscribe({
        next: () => {
          const resume: Partial<Resume> = {}
            resume.name = this.formGroup.controls['name'].value
            resume.title = this.formGroup.controls['title'].value

            if (this.formGroup.controls['birthdate'].value && this.formGroup.controls['birthdate'].value instanceof Date) {
              const birthDate = (this.formGroup.controls['birthdate'].value as Date).toLocaleDateString('en-US')

              resume.birthdate = birthDate
            }

            resume.residence = this.formGroup.controls['residence'].value

            this.resumeService.updateResume(resume)
        }
      })
  }

}
