import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-skill-table',
  templateUrl: './skill-table.component.html',
  styleUrls: ['./skill-table.component.scss']
})
export class SkillTableComponent implements OnInit {

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  @Output()
  onDelete: EventEmitter<FormGroup> = new EventEmitter<FormGroup>()

  @Output()
  onUpdate: EventEmitter<void> = new EventEmitter<void>()

  constructor() { }

  ngOnInit(): void {
  }

  deleteSkillTable() {
    this.onDelete.emit(this.formGroup)
    this.onDelete.complete()
  }

  get titleCode(): FormControl {
    return this.formGroup.get('title_code') as FormControl
  }

  get skills(): FormGroup[] {
    const skills: FormGroup[] = []

    if (this.formArray) {
      for (let i = 0; i < this.formArray.length; i++) {
        skills.push(this.formArray.at(i) as FormGroup)
      }
    }

    return skills
  }

  addSkill() {
    const formGroup: FormGroup = new FormGroup({
      name: new FormControl('', [ Validators.required ]),
      stars: new FormControl('', [ Validators.required ]),
      since: new FormControl('', [ Validators.required ])
    })

    this.formArray.push(formGroup)
  }

  get formArray(): FormArray {
    return this._formGroup.get('skills') as FormArray
  }

  onDeleteSkill(skill: FormGroup) {
    this.formArray.controls = this.formArray.controls.filter((c) => c !== skill)

    this.onUpdate.emit()
  }

}
