import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { FormGroup } from '@angular/forms'

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit {

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  @Output()
  onDelete: EventEmitter<FormGroup> = new EventEmitter<FormGroup>()

  constructor() { }

  ngOnInit(): void {
  }

  deleteSkill() {
    this.onDelete.emit(this.formGroup)
    this.onDelete.complete()
  }
}
