import { Component, Input, OnInit } from '@angular/core'
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms'

import { COMMA, ENTER } from '@angular/cdk/keycodes'

import { MatChipInputEvent } from '@angular/material/chips'
import { ResumeService } from 'src/app/resume.service'
import { Resume } from 'src/app/common/resume'

@Component({
  selector: 'app-blocks',
  templateUrl: './blocks.component.html',
  styleUrls: ['./blocks.component.scss']
})
export class BlocksComponent implements OnInit {

  readonly separatorKeysCodes = [ ENTER, COMMA ] as const

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  @Input()
  set items(value: string[][]) {
    this._items = value
  }

  get items(): string[][] {
    return this._items
  }

  private _items: string[][] = []

  get blocks(): FormGroup[] {
    const blocks: FormGroup[] = []

    if (this.formGroup) {
      for (let i = 0; i < this.formArray.length; i++) {
        blocks.push(this.formArray.at(i) as FormGroup)
      }
    }
    return blocks
  }

  constructor(
    private resumeService: ResumeService
  ) { }

  ngOnInit(): void {
    this.resumeService
      .reload
      .subscribe({
        next: (res) => {
          const resume: Resume = this.resumeService.resume

          this.items = [
            resume.block1.list,
            resume.block2.list
          ]
          console.log('items', this.items)

        }
      })

    this.formGroup
      .valueChanges
      .subscribe({
        next: () => {
          this.updateResume()
        }
      })
  }

  asFormControl(value: AbstractControl | null): FormControl {
    return value as FormControl
  }

  removeFromBlock(index: number, item: string) {
    this.items[index] = this.items[index].filter((i) => i !== item)
    this.updateResume()
  }
  
  addToBlock(index: number, $event: MatChipInputEvent) {
    if (!this.items[index]) {
      this.items[index] = []
    }
    this.items[index].push($event.value)
    $event.chipInput!.clear()
    this.updateResume()
  }

  addBlock() {
    this.formArray.push(
      new FormGroup({
        title: new FormControl('', [ Validators.required ]),
      })
    )

    this.items.push([])
  }

  deleteBlock(index: number) {
    this.formArray.removeAt(index)
    this.items = this.items.filter((entries, i) => i !== index)

    this.updateResume()
  }

  get formArray(): FormArray {
    return this.formGroup.get('blocks') as FormArray
  }

  private updateResume() {
    const resume: Partial<Resume> = {}
    if (!resume.block1) {
      resume.block1 = {
        title: '',
        list: []
      }
    }

    if (!resume.block2) {
      resume.block2 = {
        title: '',
        list: []
      }
    }

    const block1: FormGroup = this.blocks[0]
    const block2: FormGroup = this.blocks[1]

    resume.block1.title = block1.controls['title'].value
    resume.block1.list = this._items[0]

    resume.block2.title = block2.controls['title'].value
    resume.block2.list = this._items[1]

    this.resumeService.updateResume(resume)
  
  }  
}
