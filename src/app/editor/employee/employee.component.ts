import { Component, Input, OnInit } from '@angular/core'

import { FormGroup } from '@angular/forms'
import { Resume } from 'src/app/common/resume'
import { ResumeService } from 'src/app/resume.service'

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  @Input()
  set formGroup(value: FormGroup) {
    this._formGroup = value
  }

  get formGroup(): FormGroup {
    return this._formGroup
  }

  private _formGroup!: FormGroup

  constructor(
    private resumeService: ResumeService
  ) { }

  ngOnInit(): void {
    this.formGroup
    .valueChanges
    .subscribe({
      next: () => {
          const resume: Partial<Resume> = {
            employee: {
              introduction: []
            }
          }

          resume.employee!.introduction = this.formGroup.controls['introduction'].value.split('\n')

          this.resumeService.updateResume(resume)
        }
      })
  }

}
