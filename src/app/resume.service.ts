import { Injectable } from '@angular/core'

import { BehaviorSubject, Observable } from 'rxjs'

import { Resume } from './common/resume'

@Injectable({
  providedIn: 'root'
})
export class ResumeService {

  private $resume: BehaviorSubject<Resume>

  private _resume: Resume

  private $reload: BehaviorSubject<boolean>

  constructor() { 
    this._resume = this.buildResume()
    this.$resume = new BehaviorSubject<Resume>(this._resume)
    this.$reload = new BehaviorSubject<boolean>(false)
  }

  get resume(): Resume {
    return this._resume
  }

  get resumeObservable(): Observable<Resume> {
    return this.$resume.asObservable()
  }

  get reload(): Observable<boolean> {
    return this.$reload.asObservable()
  }

  updateResume(resume: Partial<Resume>) {
    this._resume = {
      ...this._resume,
      ...resume
    }
    this.$resume.next(this._resume)
  }

  private buildResume(): Resume {
    return {
      language: 'EN',
      name: '',
      title: 'Java Developer',
      birthdate: '',
      residence: '',
      employee: {
        introduction: []
      },
      block1: {
        title: 'Education',
        list: []
      },
      block2: {
        title: 'Certifications',
        list: []
      },
      technical_skills: {
        left_tables: [
          {
            title_code: 'programming_languages',
            skills: []
          },
          {
            title_code: 'frameworks_libs',
            skills: []
          },
          {
            title_code: 'databases',
            skills: []
          },
          {
            title_code: 'methods_concepts',
            skills: []
          }
        ],
        right_tables: [
          {
            title_code: 'tools',
            skills: []
          },
          {
            title_code: 'platforms',
            skills: []
          },
          {
            title_code: 'ide',
            skills: []
          }
        ]
      },
      project_history: {
        break_before: true,
        pages: []
      }
    }
  }

  updateResumeFromViewer(resume: Resume) {
    this._resume = resume
    this.$resume.next(this._resume)
    this.$reload.next(true)
  }
}
