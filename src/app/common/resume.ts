
export interface Resume {
    language: string
    name: string
    title: string
    birthdate: string
    residence: string
    image?: string
    employee: {
        introduction: string[]
    }
    block1: {
        title: string
        list: string[]
    }
    block2: {
        title: string
        list: string[]
    }
    technical_skills: {
        left_tables: {
            title_code: string
            skills: {
                name: string
                stars: number
                since: string
            }[]
        }[]
        right_tables: {
            title_code: string
            skills: {
                name: string
                stars: number
                since: string
            }[]
        }[]
    }
    project_history: {
        break_before: boolean
        pages: {
            aside: boolean
            content: {
                title: string
                period: string
                client: string
                description: string[]
                technologies: string[]
            }[]
        }[]
    }
}
