import { Component, OnInit, ViewEncapsulation } from '@angular/core'

import { parse, stringify } from 'yaml'

import { Resume } from '../common/resume'
import { ResumeService } from '../resume.service'

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {
  
  resume!: Resume

  model: string = ''

  options: any

  constructor(
    private resumeService: ResumeService
  ) { 
    this.options = {
      lineNumbers: true,
      theme: 'material',
      mode: 'yaml'
    }
  }

  ngOnInit(): void {
    this.resumeService
      .resumeObservable
      .subscribe({
        next: (res) => {
          this.resume = res
          this.model = stringify(res)
        }
      })
  }

  handleChange($evt: Event) {
    const resume: Resume = parse(this.model)

    this.resumeService.updateResumeFromViewer(resume)
  }


}
