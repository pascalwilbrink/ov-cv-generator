import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { FlexLayoutModule } from '@angular/flex-layout'
import { CodemirrorModule } from '@ctrl/ngx-codemirror'

import { MatCardModule } from '@angular/material/card'
import { MatInputModule } from '@angular/material/input'
import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatNativeDateModule } from '@angular/material/core'
import { MatChipsModule } from '@angular/material/chips'
import { MatIconModule } from '@angular/material/icon'
import { MatButtonModule } from '@angular/material/button'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatStepperModule } from '@angular/material/stepper'
import { MatCheckboxModule } from '@angular/material/checkbox'
import { MatSnackBarModule } from '@angular/material/snack-bar'

import { AppComponent } from './app.component'
import { ViewerComponent } from './viewer/viewer.component'
import { EditorComponent } from './editor/editor.component'
import { SkillComponent } from './editor/skill/skill.component'
import { BasicComponent } from './editor/basic/basic.component'
import { EmployeeComponent } from './editor/employee/employee.component'
import { BlocksComponent } from './editor/blocks/blocks.component'
import { SkillsComponent } from './editor/skills/skills.component'
import { SkillTableComponent } from './editor/skill-table/skill-table.component'
import { ProjectHistoryComponent } from './editor/project-history/project-history.component'
import { PageComponent } from './editor/project-history/page/page.component'
import { ProjectComponent } from './editor/project-history/project/project.component'

@NgModule({
  declarations: [
    AppComponent,
    ViewerComponent,
    EditorComponent,
    SkillComponent,
    BasicComponent,
    EmployeeComponent,
    BlocksComponent,
    SkillsComponent,
    SkillTableComponent,
    ProjectHistoryComponent,
    PageComponent,
    ProjectComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    CodemirrorModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatStepperModule,
    MatCheckboxModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
